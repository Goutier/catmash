module Client.Router
open Elmish.Browser.UrlParser
open Elmish.Browser.Navigation


type Route =
    | Duel
    | CatList


let optionToHash =
    function
    | Some Duel -> "#duel"
    | Some CatList -> "#cats"
    | None -> "#not-found"


let toHash =
    Some >> optionToHash


let pageParser(): Parser<Route->_,_> =
    oneOf [
        map CatList (s "cats")
        map Duel (s "duel")
        map Duel top
    ]
