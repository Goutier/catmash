module Client.App

open Fable.Core
open Fable.Import
open Elmish
open Elmish.React
open Elmish.Browser.Navigation
open Elmish.Browser.UrlParser
open Client.Router
open Elmish.Debug

type Page =
    | Duel of Duel.Component.Model
    | CatList of CatList.Component.Model
    | NotFound

type Model =
  { Page : Page }

type AppMsg =
    | DuelMsg of Duel.Component.Msg
    | CatListMsg of CatList.Component.Msg

// handle effect of url update as classic message do
let newPage result =
    match result with
    | None ->
        Browser.console.error("Error parsing url")
        NotFound, Cmd.none

    | Some Route.Duel ->
        let m, cmd = Duel.Component.init
        Page.Duel m, Cmd.map DuelMsg cmd

    | Some Route.CatList ->
        let m, cmd = CatList.Component.init
        Page.CatList m, Cmd.map CatListMsg cmd

let urlUpdate (result:Route option) model =
    let page, cmd = newPage result
    { model with Page = page }, cmd


let init result =
    let p, cmd = newPage result
    { Page = p }, cmd

let update msg model =
    match msg, model.Page with
    | AppMsg.CatListMsg m, Page.CatList p ->
        let newModel, cmd = CatList.Component.update m p
        { model with Page = Page.CatList newModel }, Cmd.map CatListMsg cmd

    | AppMsg.DuelMsg m, Page.Duel p ->
        let newModel, cmd = Duel.Component.update m p
        { model with Page = Page.Duel newModel }, Cmd.map DuelMsg cmd

    | _, _ ->
        model, Cmd.none

let view model (dispatch:AppMsg -> unit) =
    match model.Page with
    | Page.Duel m ->
        Duel.Component.view m (dispatch << DuelMsg)

    | Page.CatList m ->
        CatList.Component.view m dispatch

    | Page.NotFound ->
        NotFound.view


// Startup
Program.mkProgram init update view
    |> Program.toNavigable (parseHash (pageParser())) urlUpdate
    |> Program.withConsoleTrace
    |> Program.withReact "catmash-app"
#if DEBUG
    |> Program.withDebugger
#endif
    |> Program.run