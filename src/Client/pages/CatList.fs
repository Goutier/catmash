module Client.CatList.Component
open Elmish
open Elmish.React
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Server.Domain
open Fable.PowerPack
open Fable.PowerPack.Fetch.Fetch_types
open Server
open Client.Router


type Msg =
    | CatListLoaded of Cat list
    | LoadingFailed of exn


type Model =
    | Error of string
    | Loading
    | WithContent of Cat list


let loadCats() =
    let url = Paths.allCats

    Fetch.fetchAs<Cat list> url []


let loadCatsCmd =
    Cmd.ofPromise loadCats () CatListLoaded LoadingFailed


let init =
    Loading, loadCatsCmd


let update msg model =
    match msg with
    | CatListLoaded cats ->
        WithContent cats, Cmd.none

    | LoadingFailed e ->
        Error (e.Message), Cmd.none

// VIEW

let viewCat cat =
    li [ClassName "cat"] [
        div [ClassName "cat-score"] [str <| string cat.Score]
        img [ Src <| Picture.toString cat.Picture
              ClassName "cat-picture"
            ]
    ]


let view model dispatch =
    match model with
    | Loading  ->
        div [] [ str "Loading" ]

    | WithContent cats ->
        div [] [
            a [Href <| toHash Duel; ClassName "duel-link"] [str "Return to duel"]
            ul [] <| List.map viewCat cats
        ]

    | Error reason ->
        div [] [ str reason ]