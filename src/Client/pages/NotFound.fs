module Client.NotFound
open Elmish.React
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props

let view =
    div [] [str "Not Found"]