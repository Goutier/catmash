module Client.Duel.Component

open Elmish
open Elmish.React
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.PowerPack
open Fable.PowerPack.Fetch.Fetch_types
open Server.Domain
open Client.Router
open Server.Domain
open Server

type Msg =
    | Loaded of Duel
    | DuelSettled of DuelResult
    | Failed of exn
    | NoOp


type Model  =
    | Loading
    | WithContent of Duel
    | LoadingNew of Duel
    | Error of string


let saveResult cat =
    let url = Paths.saveDuelResult

    Fetch.postRecord url cat []


let loadDuel() =
    let url = Paths.randomDuel

    Fetch.fetchAs<Duel> url []


let init =
    Loading, Cmd.ofPromise loadDuel () Loaded Failed


let update msg model =
    match msg, model with
    | Loaded duel, _ ->
        WithContent duel, Cmd.none

    | DuelSettled result, WithContent d ->
        LoadingNew d, Cmd.batch [
            Cmd.ofPromise loadDuel () Loaded Failed
            Cmd.ofPromise saveResult result (fun _ -> NoOp) Failed
        ]

    | Failed e, _ ->
        Error <| e.Message, Cmd.none

    | NoOp, _ ->
        model, Cmd.none

    | _,_ ->
        model, Cmd.none

// VIEW

let viewCat cat opponent enabled dispatch =
    let onClick msg =
        OnClick <| fun _ -> dispatch msg

    img [
        yield classList
            [ "dueling-cat", true
              "active-dueling-cat", enabled
            ] :> IHTMLProp

        yield Src <| Picture.toString cat.Picture:>IHTMLProp

        if enabled then
            yield onClick <| DuelSettled (Cat.toResult cat opponent):>IHTMLProp
    ]


let catListLink dispatch =
    div [ClassName "cat-list-link"] [
        a [ Href <| toHash Route.CatList ] [str "See all cats"]
    ]


let catTitle =
    div [ClassName "duel-title"] [
        div [] [str "Choose the cutest !"]
    ]


let viewDuel (Duel (c1, c2)) dispatch =
    div [ClassName "duel"] [
        catTitle
        catListLink dispatch
        viewCat c1 c2 true dispatch
        viewCat c2 c2 true dispatch
    ]


let viewOldDuel (Duel (c1, c2)) dispatch =
    div [ClassName "duel"] [
        viewCat c1 c2 false dispatch
        viewCat c2 c2 false dispatch
    ]


let view model dispatch =
    match model with
    | Loading ->
        div [] [ str "Loading..." ]

    | WithContent duel ->
        viewDuel duel dispatch

    | LoadingNew duel ->
        viewOldDuel duel dispatch
    | Error reason ->
        div [] [ str <| "Error : " + reason ]