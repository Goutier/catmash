module Server.Random

let private random = System.Random()


let shuffle a =
    let array = Array.copy a

    let swap (a: _[]) x y =
        let tmp = a.[x]
        a.[x] <- a.[y]
        a.[y] <- tmp

    array |> Array.iteri (fun i _ ->
        swap array i <| random.Next(i, Array.length array)
    )
    array


let takeFromArray i =
    shuffle >> Array.take i

let takeFromList i =
    List.toArray >> takeFromArray i

let fromArray a =
    takeFromArray 1 a


let doForRandomInList x action =
    takeFromList >> action

let pairFromList xs =
    let rs = Array.toList <| takeFromList 2 xs

    match rs with
    | r1::r2::_ -> Some (r1,r2)
    | _ -> None