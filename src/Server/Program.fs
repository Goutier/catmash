﻿/// Server program entry point module.
module Server.Program

open System.IO

let GetEnvVar name =
    match System.Environment.GetEnvironmentVariable(name) with
    | null -> None
    | value -> Some value

let getPortsOrDefault defaultVal =
    GetEnvVar "PORT"
        |> Option.map uint16
        |> Option.defaultValue defaultVal

let getClientPath args =
    match args |> Array.toList with
    | clientPath :: _ when Directory.Exists clientPath -> clientPath
    | _ ->
        let devPath = Path.Combine("..","Client")
        if Directory.Exists devPath then devPath else
        @"./client"


[<EntryPoint>]
let main args =
    try
        let clientPath = getClientPath args

        WebServer.start (Path.GetFullPath clientPath) (getPortsOrDefault 8085us)
        0
    with
    | exn ->
        let color = System.Console.ForegroundColor
        System.Console.ForegroundColor <- System.ConsoleColor.Red
        System.Console.WriteLine(exn.Message)
        System.Console.ForegroundColor <- color
        1