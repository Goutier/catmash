module Server.Db
//Fake module to simulate the Database
open Server.Domain
open System.Collections.Concurrent
open Server.Http


type private CatDb = {
    CatId:CatId
    Picture:Picture
}
let private dbFromJson cat: CatDb = {
    CatId = CatId.from cat.id
    Picture = Picture.from cat.url
}

let mutable private cats: CatDb list option = None
let private initData() =
        let result =
            Http.getCats
                |> Async.RunSynchronously
                |> List.map dbFromJson


        cats <- Some result
        result

let private getData():CatDb list =
    match cats with
    | None -> initData()
    | Some c -> c


let private toCatDb (cat:Cat):CatDb = {
    CatId = cat.Id
    Picture = cat.Picture
}

type private ResultDb = {
    Winner: CatDb
    Looser: CatDb
}

let private toResultDb (result: DuelResult) = {
    Winner = toCatDb result.Winner
    Looser = toCatDb result.Looser
}


let private results = ConcurrentBag<ResultDb>()


let private getScoreOf cat:int<score> =
    let wonBy c d = d.Winner = c

    results
        |> Seq.filter (wonBy cat)
        |> Seq.length
        |> LanguagePrimitives.Int32WithMeasure

let private toDomainCat cat =
    { Id = cat.CatId
      Picture = cat.Picture
      Score =  getScoreOf cat
    }


let getAllCats() =
    List.map toDomainCat (getData())


let saveDuelResult result =
    results.Add(toResultDb result)
    ()