/// Functions for managing the Suave web server.
module Server.WebServer

open System.IO
open Suave
open Suave.Logging
open Suave.Operators
open Suave.Filters
open Suave.RequestErrors
open System.Net
open Server.Domain
open Server.SuaveHelpers
open Server.Domain
open Server.AppRoutes

let statics =
    GET >=> choose [
        path "/" >=> Files.browseFileHome "index.html"
        pathRegex @"/(public|js|css|Images)/(.*)\.(css|png|gif|jpg|js|map)" >=> Files.browseHome
    ]

let appRoutes =
    choose [
        GET >=> choose [
            path Paths.allCats >=> jsonQuery (getAllCats) ()
            path Paths.randomDuel >=> jsonQuery (randomDuel) ()
        ]
        POST >=> choose [
            path Paths.saveDuelResult >=> withJson<DuelResult> (jsonQuery saveDuelResult)
        ]
    ]


// Fire up our web server!
let start clientPath port =
    if not (Directory.Exists clientPath) then
        failwithf "Client-HomePath '%s' doesn't exist." clientPath

    let logger = Logging.Targets.create Logging.Info [| "Suave" |]
    let serverConfig =
        { defaultConfig with
            logger = Targets.create LogLevel.Debug [|"Server"; "Server" |]
            homeFolder = Some clientPath
            bindings = [ HttpBinding.create HTTP (IPAddress.Parse "0.0.0.0") port] }


    let app =
        choose [
            statics
            appRoutes
            NOT_FOUND "Page not found..."
        ] >=> logWithLevelStructured Logging.Info logger logFormatStructured

    startWebServer serverConfig app
