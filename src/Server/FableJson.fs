/// Functions to serialize and deserialize JSON, with client side support.
module Server.FableJson

open Newtonsoft.Json
open Newtonsoft.Json.Serialization

// Use cache, better to use only one for performance reason
let private jsonConverter = Fable.JsonConverter() :> JsonConverter

type private LowerCaseConverter() =
    inherit DefaultContractResolver()
    override this.ResolvePropertyName propertyName =
        propertyName.ToLowerInvariant()

let toJson value =
    JsonConvert.SerializeObject(value, [|jsonConverter|])

let ofJson<'a> (json:string) : 'a =
    JsonConvert.DeserializeObject<'a>(json, [|jsonConverter|])
