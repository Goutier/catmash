module Server.AppRoutes
open Server.Domain
open Server.Db
open Server.Random
open Server.SuaveHelpers

// Orchestrate the effect of the views
// doens't care about how the Web API look like

let getAllCats =
    Cat.sortByScore << Db.getAllCats


let randomDuel =
    Option.map Duel << Random.pairFromList << Db.getAllCats


let saveDuelResult =
    Db.saveDuelResult