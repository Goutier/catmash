module Server.Http

open Server.FableJson

type CatJson = {
    id:string
    url:string
}
type CatJsonFile = {
    images:CatJson list
}

let get (url:string) =
    let httpClient = new System.Net.Http.HttpClient()
    httpClient.GetAsync(url)
        |> Async.AwaitTask

let getString url =
    async {
        let! t = get url
        return! t.Content.ReadAsStringAsync()
            |> Async.AwaitTask
    }

let urlWorks url =
    async {
        let! response = get url

        return response.IsSuccessStatusCode
    }

let validCat cat =
    async {
        try
            let! works = urlWorks cat.url

            if works then
                return Some cat
            else
                return None
        with
        | _ -> return None
    }

let getCats =
    async {
        let! requests = getString "https://latelier.co/data/cats.json"

        let! result =
            requests
                |> FableJson.ofJson
                |> fun f -> f.images
                |> List.toSeq
                |> Seq.map validCat
                |> Async.Parallel

        return Array.choose id result
            |> Array.toList
    }
