/// Domain model shared between client and server.
module Server.Domain

open System


[<Measure>] type score
type Picture = Picture of string
type CatId = CatId of string


type Cat = {
    Picture: Picture
    Id: CatId
    Score:int<score>
}


type DuelResult = {
    Winner:Cat
    Looser:Cat
}


type Duel = Duel of Cat * Cat

module Duel =
    let fromValues c1 c2 = c1, c2

module Picture =
    let toString (Picture p) = p
    let from = Picture


module CatId =
    let from = CatId
    let toString (CatId c) = c.ToString()




module Cat =
    let score c = c.Score
    let picture c = c.Picture
    let id c = c.Id

    let toResult winner looser = {
        Winner = winner
        Looser = looser
    }

    let sortByScore =
        List.sortByDescending score
