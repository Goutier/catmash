module Server.SuaveHelpers
open Suave
open Suave.Filters
open Suave.Operators
open Suave.RequestErrors

let jsonQuery getResult param: WebPart =
    fun (context:HttpContext) ->
        let result = getResult param

        Successful.OK (FableJson.toJson result) context

let withJson<'a> (run:'a -> WebPart): WebPart =
    fun (context:HttpContext) ->
        let param =
            context.request.rawForm
                |> System.Text.Encoding.UTF8.GetString
                |> FableJson.ofJson<'a>

        run param context